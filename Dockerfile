FROM elixir:1.10

ARG NODE_VERSION=12.18.3

ARG NODE_TARBALL=https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz

RUN  mkdir /node \
  && curl ${NODE_TARBALL} -so node.tar.xz \
  && tar --extract --xz -f node.tar.xz -C /node --strip 1 \
  && rm node.tar.xz

ENV PATH /node/bin:${PATH}

ARG PHX_VERSION=1.5.4

RUN mix local.hex --force

RUN mix archive.install hex phx_new ${PHX_VERSION} --force

WORKDIR /gogls
