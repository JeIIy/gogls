# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :gogls,
  ecto_repos: [Gogls.Repo]

# Configures the endpoint
config :gogls, GoglsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "KXYB4wLgFfVNrdemt2+84JsrTuW2A8CPZLL9ZBV7WhixX67Z1BzsnO7G8M07+eEP",
  render_errors: [view: GoglsWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Gogls.PubSub,
  live_view: [signing_salt: "ra8vP/RU"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
